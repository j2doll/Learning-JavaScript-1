// 02.group-the-strings.js

const words = ["Beachball", "Rodeo", "Angel",
"Aardvark", "Xylophone", "November", "Chocolate",
"Papaya", "Uniform", "Joker", "Clover", "Bali"];

const alphabetical = words.reduce(
    (a, x) => 
        { // return value of a
            if ( ! a[x[0]] ) // if property(x[0]) of a is defined,
                a[x[0]] = []; // create new empty property using x[0]

            a[x[0]].push(x); // push value to property(x[0]) of a
            return a; 
        },
        {} // initial value of a : it's empty
);



