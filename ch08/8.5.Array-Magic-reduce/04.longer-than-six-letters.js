// 04.longer-than-six-letters.js

const words = ["Beachball", "Rodeo", "Angel",
"Aardvark", "Xylophone", "November", "Chocolate",
"Papaya", "Uniform", "Joker", "Clover", "Bali"];

const longWords = words.reduce(
    (a, w) => 
        w.length > 6 ? (a+" "+w) : a, // return value (of a)
        "" // initial value (of a)
    ).trim();

console.log(longWords);
// Beachball Aardvark Xylophone November Chocolate Uniform

