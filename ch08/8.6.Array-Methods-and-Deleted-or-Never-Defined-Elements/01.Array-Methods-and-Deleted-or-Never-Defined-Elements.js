// 01.Array-Methods-and-Deleted-or-Never-Defined-Elements.js

const arr = [1, 2, 3, 4, 5];
delete arr[2];

let ret = arr.map( x => 0 ); // [0, 0, <1 empty slot>, 0, 0]

console.log( ret ); // [0, 0, …, 0, 0]
